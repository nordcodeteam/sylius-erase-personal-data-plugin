# Sylius Erase Personal Data Plugin

This plugin allows customers to request their personal data to be erased from a Sylius store, and for the store administrator to do this.

## Installation

1. Install the package via composer:
    ```shell script
    $ composer require nordcode.io/sylius-erase-personal-data-plugin
    ```
2. Update `config/bundles.php`:
    ```php
    <?php
    
    return [
    
    ...
    
    Nordcode\SyliusErasePersonalDataPlugin\NordcodeSyliusErasePersonalDataPlugin::class => ['all' => true],
    ]
    ``` 
3. Update your `Customer` model class to implement our `CustomerInterface`:
    ```php
    ...
    use Nordcode\SyliusErasePersonalDataPlugin\Entity\CustomerInterface;
    use Nordcode\SyliusErasePersonalDataPlugin\Entity\CustomerTrait;
   
    ...
    
    class Customer extends BaseCustomer implements CustomerInterface
    {
        use CustomerTrait;
    
       ...
    }
    ``` 
4. Include plugin routes in your route configuration (e.g. `config/routes.yaml`):
    ```yaml
    nordcode_erase_personal_data_admin:
        resource: "@NordcodeSyliusErasePersonalDataPlugin/Resources/config/admin_routing.yaml"
        prefix: /admin

    nordcode_erase_personal_data_shop:
        resource: "@NordcodeSyliusErasePersonalDataPlugin/Resources/config/shop_routing.yaml"
    ```
5. Copy and execute the migrations from `src/Migrations/`:
    ```shell script
    $ cp vendor/nordcode.io/sylius-erase-personal-data-plugin/src/Migrations/* src/Migrations/
    $ bin/console doctrine:migrations:migrate
    ```
   **Note:** If you are running it on production, add the `-e prod` flag to this command.

## Usage
**Note:** When erasing personal data, an attempt will be made to delete the customer and their respective user altogether.

The plugin adds a new route for customers to request their data to be removed. How this is handled is configurable: either all data may be erased immediately, or the newly added `erasureRequestedAt` field updated to current timestamp. 

Also an admin route is added, which allows the store administrator to erase customer's personal data when necessary. This may be done from customer's profile page. In addition, the customer grid is extended with an additional column and filter.

Erasure is done in steps and it's possible to hook to each step, or even cancel it entirely. Have a look at `src/Services/PersonalDataEraser.php` and the `src/Event/*` to see how it works. 
