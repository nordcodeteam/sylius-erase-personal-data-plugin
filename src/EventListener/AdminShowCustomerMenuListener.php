<?php
declare(strict_types=1);

namespace Nordcode\SyliusErasePersonalDataPlugin\EventListener;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

final class AdminShowCustomerMenuListener
{
    public function __invoke(MenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();
        $customerId = $event->getCustomer()->getId();

        $menu->addChild(
            'erase_personal_data', [
                'route' => 'nordcode_sylius_erase_personal_data_plugin_admin_erase_personal_data',
                'routeParameters' => ['id' => $customerId],
            ])
            ->setAttributes([
                'type' => 'transition',
                'confirmation' => true,
            ])
            ->setLabelAttributes([
                'color' => 'grey',
                'icon' => 'user secret',
            ])
            ->setLabel('nordcode_sylius_erase_personal_data_plugin.admin.erase_personal_data')
        ;
    }
}
