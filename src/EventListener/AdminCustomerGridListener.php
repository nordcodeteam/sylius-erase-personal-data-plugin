<?php

namespace Nordcode\SyliusErasePersonalDataPlugin\EventListener;

use Sylius\Component\Grid\Definition\Field;
use Sylius\Component\Grid\Definition\Filter;
use Sylius\Component\Grid\Event\GridDefinitionConverterEvent;

final class AdminCustomerGridListener
{
    public function __invoke(GridDefinitionConverterEvent $event): void
    {
        $grid = $event->getGrid();

        $field = Field::fromNameAndType('erasureRequestedAt', 'twig');
        $field->setLabel('nordcode_sylius_erase_personal_data_plugin.admin.erasure_requested_at');
        $field->setOptions(['template' => '@NordcodeSyliusErasePersonalDataPlugin/Admin/Grid/Field/requestedErasureAt.html.twig']);

        $grid->addField($field);

        $filter = Filter::fromNameAndType('erasureRequestedAt', 'exists');
        $filter->setLabel('nordcode_sylius_erase_personal_data_plugin.admin.erasure_requested_at');
        $grid->addFilter($filter);
    }
}
