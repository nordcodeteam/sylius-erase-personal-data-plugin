<?php
declare(strict_types=1);

namespace Nordcode\SyliusErasePersonalDataPlugin\Event;

use Sylius\Component\Core\Model\ShopUserInterface;

class BeforeEraseShopUserPersonalDataEvent extends AbstractCancellableEvent
{
    /** @var ShopUserInterface */
    private $shopUser;

    public function __construct(ShopUserInterface $shopUser)
    {
        $this->shopUser = $shopUser;
    }

    public function getShopUser(): ShopUserInterface
    {
        return $this->shopUser;
    }

    public function setShopUser(ShopUserInterface $shopUser): void
    {
        $this->shopUser = $shopUser;
    }
}
