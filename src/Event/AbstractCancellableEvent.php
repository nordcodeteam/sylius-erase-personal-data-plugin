<?php
declare(strict_types=1);

namespace Nordcode\SyliusErasePersonalDataPlugin\Event;

use Symfony\Contracts\EventDispatcher\Event;

class AbstractCancellableEvent extends Event
{
    /** @var bool */
    private $cancelled = false;

    public function isCancelled(): bool
    {
        return $this->cancelled;
    }

    public function setCancelled(bool $cancelled): void
    {
        $this->cancelled = $cancelled;
    }
}
