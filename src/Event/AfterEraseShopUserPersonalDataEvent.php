<?php
declare(strict_types=1);

namespace Nordcode\SyliusErasePersonalDataPlugin\Event;

use Sylius\Component\Core\Model\ShopUserInterface;
use Symfony\Contracts\EventDispatcher\Event;

class AfterEraseShopUserPersonalDataEvent extends Event
{
    /** @var ShopUserInterface */
    private $shopUser;

    public function __construct(ShopUserInterface $shopUser)
    {
        $this->shopUser = $shopUser;
    }

    public function getShopUser(): ShopUserInterface
    {
        return $this->shopUser;
    }

    public function setShopUser(ShopUserInterface $shopUser): void
    {
        $this->shopUser = $shopUser;
    }
}
