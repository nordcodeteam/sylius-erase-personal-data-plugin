<?php
declare(strict_types=1);

namespace Nordcode\SyliusErasePersonalDataPlugin\Event;

use Sylius\Component\Core\Model\OrderInterface;

class CheckIfOrderIsCompletedEvent extends AbstractCancellableEvent
{
    /** @var OrderInterface */
    private $order;

    /** @var bool|null */
    private $isOrderCompleted;

    public function __construct(OrderInterface $order)
    {
        $this->order = $order;
    }

    public function getOrder(): OrderInterface
    {
        return $this->order;
    }

    public function setOrder(OrderInterface $order): void
    {
        $this->order = $order;
    }

    public function isOrderCompleted(): ?bool
    {
        return $this->isOrderCompleted;
    }

    public function setIsOrderCompleted(?bool $isOrderCompleted): void
    {
        $this->isOrderCompleted = $isOrderCompleted;
    }
}
