<?php
declare(strict_types=1);

namespace Nordcode\SyliusErasePersonalDataPlugin\Event;

use Sylius\Component\Core\Model\OrderInterface;
use Symfony\Contracts\EventDispatcher\Event;

class AfterEraseOrderPersonalDataEvent extends Event
{
    /** @var OrderInterface */
    private $order;

    public function __construct(OrderInterface $order)
    {
        $this->order = $order;
    }

    public function getOrder(): OrderInterface
    {
        return $this->order;
    }

    public function setOrder(OrderInterface $order): void
    {
        $this->order = $order;
    }
}
