<?php
declare(strict_types=1);

namespace Nordcode\SyliusErasePersonalDataPlugin\Event;

use Sylius\Component\Core\Model\AddressInterface;
use Symfony\Contracts\EventDispatcher\Event;

class AfterEraseAddressPersonalDataEvent extends Event
{
    /** @var AddressInterface */
    private $address;

    public function __construct(AddressInterface $address)
    {
        $this->address = $address;
    }

    public function getAddress(): AddressInterface
    {
        return $this->address;
    }

    public function setAddress(AddressInterface $address): void
    {
        $this->address = $address;
    }
}
