<?php
declare(strict_types=1);

namespace Nordcode\SyliusErasePersonalDataPlugin\Controller\Shop\Action;

use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\View;
use Nordcode\SyliusErasePersonalDataPlugin\Entity\CustomerInterface;
use Nordcode\SyliusErasePersonalDataPlugin\Event\AfterProcessCustomerPersonalDataErasureRequestEvent;
use Nordcode\SyliusErasePersonalDataPlugin\Event\BeforeProcessCustomerPersonalDataErasureRequestEvent;
use Nordcode\SyliusErasePersonalDataPlugin\Exception\CustomerHasIncompleteOrdersException;
use Nordcode\SyliusErasePersonalDataPlugin\Form\RequestPersonalDataErasureType;
use Nordcode\SyliusErasePersonalDataPlugin\Service\PersonalDataEraser;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Component\Core\Context\ShopperContextInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

final class RequestPersonalDataErasureAction extends AbstractController
{
    const POLICY_ERASE_IMMEDIATELY = 'erase_immediately';
    const POLICY_TAG_ONLY = 'tag_only';

    /** @var EntityRepository */
    private $customerRepository;

    /** @var EntityManagerInterface */
    private EntityManagerInterface $entityManager;

    /** @var ShopperContextInterface */
    private $shopperContext;

    /** @var PersonalDataEraser */
    private PersonalDataEraser $personalDataEraser;

    /** @var EventDispatcherInterface */
    private EventDispatcherInterface $eventDispatcher;

    /** @var ParameterBagInterface */
    private ParameterBagInterface $parameterBag;

    public function __construct(
        ParameterBagInterface $parameterBag,
        EntityRepository $customerRepository,
        EntityManagerInterface $entityManager,
        ShopperContextInterface $shopperContext,
        PersonalDataEraser $personalDataEraser,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->parameterBag = $parameterBag;
        $this->customerRepository = $customerRepository;
        $this->entityManager = $entityManager;
        $this->shopperContext = $shopperContext;
        $this->personalDataEraser = $personalDataEraser;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(Request $request): Response
    {
        $customer = $this->shopperContext->getCustomer();

        if ($customer === null) {
            $this->addFlash('warning', 'nordcode_sylius_erase_personal_data_plugin.shop.customer_not_in_context');
            return $this->redirectToRoute($this->getParameter('nordcode_sylius_erase_personal_data_plugin.redirect_paths.customer_not_in_context'));
        }

        if (!$customer instanceof CustomerInterface) {
            $this->addFlash('warning', 'nordcode_sylius_erase_personal_data_plugin.shop.customer_misconfigured');
            return $this->redirectToRoute($this->getParameter('nordcode_sylius_erase_personal_data_plugin.redirect_paths.customer_not_in_context'));
        }

        $form = $this->createForm(RequestPersonalDataErasureType::class);
        $form->handleRequest($request);

        if (!($form->isSubmitted() && $form->isValid())) {
            if ($request->isXmlHttpRequest()) {
                return $this->json($form);
            } else {
                return $this->render('@NordcodeSyliusErasePersonalDataPlugin/Shop/erasePersonalData.html.twig', ['form' => $form->createView()]);
            }
        }

        $beforeEvent = new BeforeProcessCustomerPersonalDataErasureRequestEvent($customer);
        $this->eventDispatcher->dispatch($beforeEvent);

        $erasurePolicy = $this->getParameter('nordcode_sylius_erase_personal_data_plugin.erase_data_request_processing_policy');

        $customer->setErasureRequestedAt(new \DateTime());
        $this->entityManager->flush();

        switch($erasurePolicy) {
            case self::POLICY_ERASE_IMMEDIATELY:
                try {
                    $this->personalDataEraser->erasePersonalData($customer);
                    $this->addFlash('success', 'nordcode_sylius_erase_personal_data_plugin.shop.personal_data_erased');
                } catch(CustomerHasIncompleteOrdersException $e) {
                    $this->addFlash('warning', 'nordcode_sylius_erase_personal_data_plugin.shop.personal_data_not_erased_due_to_existing_incomplete_orders');
                }
                break;
            case self::POLICY_TAG_ONLY:
            default:
                $this->addFlash('success', 'nordcode_sylius_erase_personal_data_plugin.shop.personal_data_erasure_requested');
                break;
        }

        $this->eventDispatcher->dispatch(new AfterProcessCustomerPersonalDataErasureRequestEvent($customer));

        return $this->redirectToRoute($this->getParameter('nordcode_sylius_erase_personal_data_plugin.redirect_paths.request_processed_successfully'));
    }
}
