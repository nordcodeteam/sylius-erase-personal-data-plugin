<?php
declare(strict_types=1);

namespace Nordcode\SyliusErasePersonalDataPlugin\Controller\Admin\Action;

use Nordcode\SyliusErasePersonalDataPlugin\Exception\CustomerHasIncompleteOrdersException;
use Nordcode\SyliusErasePersonalDataPlugin\Service\PersonalDataEraser;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Component\Core\Model\CustomerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class ErasePersonalDataAction extends AbstractController
{
    /** @var EntityRepository */
    private $customerRepository;

    /** @var PersonalDataEraser */
    private $personalDataEraser;

    public function __construct(EntityRepository $customerRepository, PersonalDataEraser $personalDataEraser)
    {
        $this->customerRepository = $customerRepository;
        $this->personalDataEraser = $personalDataEraser;
    }

    public function __invoke(Request $request): RedirectResponse
    {
        $customerId = $request->attributes->get('id');
        /** @var CustomerInterface $customer */
        $customer = $this->customerRepository->find($customerId);

        if (!$customer) {
            throw new NotFoundHttpException(sprintf('Customer with id %s has not been found', $customerId));
        }

        try {
            $this->personalDataEraser->erasePersonalData($customer);
            $this->addFlash('success', 'nordcode_sylius_erase_personal_data_plugin.admin.personal_data_erased');
        } catch(CustomerHasIncompleteOrdersException $e) {
            $this->addFlash('warning', 'nordcode_sylius_erase_personal_data_plugin.admin.personal_data_not_erased_due_to_existing_incomplete_orders');
        }

        // If customer still exists, redirect to their page, otherwise to the list
        if ($customerId = $customer->getId()) {
            return $this->redirectToRoute('sylius_admin_customer_show', ['id' => $customerId]);
        } else {
            return $this->redirectToRoute('sylius_admin_customer_index');
        }
    }

}
