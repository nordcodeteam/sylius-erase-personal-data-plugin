<?php
declare(strict_types=1);

namespace Nordcode\SyliusErasePersonalDataPlugin;

use Sylius\Bundle\CoreBundle\Application\SyliusPluginTrait;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class NordcodeSyliusErasePersonalDataPlugin extends Bundle
{
    use SyliusPluginTrait;
}
