<?php
declare(strict_types=1);

namespace Nordcode\SyliusErasePersonalDataPlugin\Service;

use Doctrine\DBAL\Exception\ConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Nordcode\SyliusErasePersonalDataPlugin\Event\AfterEraseAddressPersonalDataEvent;
use Nordcode\SyliusErasePersonalDataPlugin\Event\AfterEraseCustomerPersonalDataEvent;
use Nordcode\SyliusErasePersonalDataPlugin\Event\AfterEraseOrderPersonalDataEvent;
use Nordcode\SyliusErasePersonalDataPlugin\Event\AfterEraseShopUserPersonalDataEvent;
use Nordcode\SyliusErasePersonalDataPlugin\Event\BeforeEraseAddressPersonalDataEvent;
use Nordcode\SyliusErasePersonalDataPlugin\Event\BeforeEraseCustomerPersonalDataEvent;
use Nordcode\SyliusErasePersonalDataPlugin\Event\BeforeEraseOrderPersonalDataEvent;
use Nordcode\SyliusErasePersonalDataPlugin\Event\BeforeEraseShopUserPersonalDataEvent;
use Nordcode\SyliusErasePersonalDataPlugin\Event\CheckIfOrderIsCompletedEvent;
use Nordcode\SyliusErasePersonalDataPlugin\Exception\CustomerHasIncompleteOrdersException;
use Psr\Log\LoggerInterface;
use Sylius\Component\Core\Model\AddressInterface;
use Sylius\Component\Core\Model\Customer;
use Sylius\Component\Core\Model\CustomerInterface;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\ShopUserInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class PersonalDataEraser
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /** @var LoggerInterface|null */
    protected $logger;

    public function __construct(
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher,
        ?LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;
    }

    public function erasePersonalData(CustomerInterface $customer): void
    {
        if ($this->customerHasIncompleteOrders($customer)) {
            throw new CustomerHasIncompleteOrdersException();
        }

        if ($this->logger !== null) {
            $this->logger->info(sprintf('Erasing personal data for customer #%d.', $customer->getId()));
        }

        $user = $customer->getUser();
        if ($user) {
            $this->eraseUserPersonalData($user);
        }

        foreach ($customer->getOrders() as $order) {
            $this->eraseOrderPersonalData($order);
        }

        foreach ($customer->getAddresses() as $address) {
            $customer->removeAddress($address);
            $this->eraseAddressPersonalData($address);
        }

        $this->eraseCustomerPersonalData($customer);

        if ($this->logger !== null) {
            $this->logger->info(sprintf('Personal data has been erased for customer #%d.', $customer->getId()));
        }
    }

    private function eraseUserPersonalData(ShopUserInterface $user): void
    {
        $beforeEvent = new BeforeEraseShopUserPersonalDataEvent($user);
        $this->eventDispatcher->dispatch($beforeEvent);
        if ($beforeEvent->isCancelled()) {
            return;
        }

        $user->setEmail(sprintf('%s@erased', uniqid()));
        $user->setEmailCanonical(null);
        $user->setPassword('');
        $user->setEmailVerificationToken(null);
        $user->setUsername(null);
        $user->setUsernameCanonical(null);
        $user->setEnabled(false);
        $user->setLocked(true);

        // Trigger user flush before attempting to remove in order to avoid the "Entity is not managed" exception.
        $this->entityManager->flush();

        try {
            $this->entityManager->remove($user);
            $this->entityManager->flush();
        } catch (ConstraintViolationException $e) {
            $this->logger->warn('Got exception trying to delete user entry. Users personal data has already been erased though.', [$e]);
        }

        $this->eventDispatcher->dispatch(new AfterEraseShopUserPersonalDataEvent($user));
    }

    private function eraseOrderPersonalData(OrderInterface $order): void
    {
        $beforeEvent = new BeforeEraseOrderPersonalDataEvent($order);
        $this->eventDispatcher->dispatch($beforeEvent);
        if ($beforeEvent->isCancelled()) {
            return;
        }

        $order->setShippingAddress(null);
        $order->setBillingAddress(null);
        $order->setCustomerIp(null);
        $order->setLocaleCode('');
        $this->entityManager->flush();

        $this->eventDispatcher->dispatch(new AfterEraseOrderPersonalDataEvent($order));
    }

    private function eraseAddressPersonalData(AddressInterface $address): void
    {
        $beforeEvent = new BeforeEraseAddressPersonalDataEvent($address);
        $this->eventDispatcher->dispatch($beforeEvent);
        if ($beforeEvent->isCancelled()) {
            return;
        }

        $address->setFirstName(null);
        $address->setLastName(null);
        $address->setPhoneNumber(null);
        $address->setPostcode(null);
        $address->setCity(null);
        $address->setCompany(null);
        $address->setCountryCode(null);
        $address->setProvinceCode(null);
        $address->setProvinceName(null);
        $address->setStreet(null);

        try {
            $this->entityManager->remove($address);
            $this->entityManager->flush();
        } catch (ConstraintViolationException $e) {
            $this->logger->warn('Got exception trying to delete address entry, will try clearing data instead.', [$e]);

            $this->entityManager->persist($address);
            $this->entityManager->flush();
        }

        $this->eventDispatcher->dispatch(new AfterEraseAddressPersonalDataEvent($address));
    }

    private function eraseCustomerPersonalData(CustomerInterface $customer): void
    {
        $beforeEvent = new BeforeEraseCustomerPersonalDataEvent($customer);
        $this->eventDispatcher->dispatch($beforeEvent);
        if ($beforeEvent->isCancelled()) {
            return;
        }

        $customer->setFirstName(null);
        $customer->setLastName(null);
        $customer->setBirthday(null);
        $customer->setGender(Customer::UNKNOWN_GENDER);
        $customer->setEmail(sprintf('%s@erased', uniqid()));
        $customer->setEmailCanonical(null);
        $customer->setPhoneNumber(null);
        $customer->setSubscribedToNewsletter(false);
        $customer->setDefaultAddress(null);

        try {
            $this->entityManager->remove($customer);
            $this->entityManager->flush();
        } catch (ConstraintViolationException $e) {
            $this->logger->warn('Got exception trying to delete customer entry, will try clearing data instead.', [$e]);

            $this->entityManager->persist($customer);
            $this->entityManager->flush();
        }

        $this->eventDispatcher->dispatch(new AfterEraseCustomerPersonalDataEvent($customer));
    }

    private function customerHasIncompleteOrders(CustomerInterface $customer): bool
    {
        foreach ($customer->getOrders() as $order) {
            $event = new CheckIfOrderIsCompletedEvent($order);
            $event->setIsOrderCompleted($this->isOrderCompleted($order));
            $this->eventDispatcher->dispatch($event);

            if (!$event->isOrderCompleted()) {
                return true;
            }
        }

        return false;
    }

    private function isOrderCompleted(OrderInterface $order): bool
    {
        $completedStates = [
            OrderInterface::STATE_CANCELLED,
            OrderInterface::STATE_FULFILLED,
        ];

        return $order->getCheckoutCompletedAt() === null || in_array($order->getState(), $completedStates, true);
    }
}
