<?php
declare(strict_types=1);

namespace Nordcode\SyliusErasePersonalDataPlugin\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping\Column;

trait CustomerTrait
{
    /**
     * @Column(name="erasure_requested_at", type="datetime", nullable=true)
     *
     * @var DateTimeInterface|null
     */
    private $erasureRequestedAt;


    public function getErasureRequestedAt(): ?DateTimeInterface
    {
        return $this->erasureRequestedAt;
    }

    public function setErasureRequestedAt(?DateTimeInterface $erasureRequestedAt): void
    {
        $this->erasureRequestedAt = $erasureRequestedAt;
    }

    public function requestErasure(): void
    {
        $this->erasureRequestedAt = new DateTime();
    }
}
