<?php
declare(strict_types=1);

namespace Nordcode\SyliusErasePersonalDataPlugin\Entity;

use DateTimeInterface;

interface CustomerInterface
{
    public function getErasureRequestedAt(): ?DateTimeInterface;

    public function setErasureRequestedAt(?DateTimeInterface $removalRequestedAt): void;
}
