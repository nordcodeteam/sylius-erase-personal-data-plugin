<?php
declare(strict_types=1);

namespace Nordcode\SyliusErasePersonalDataPlugin\Exception;

class CustomerHasIncompleteOrdersException extends \RuntimeException
{
}
